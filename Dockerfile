FROM python:alpine3.11

WORKDIR /usr/src/app

ADD ./src .

ADD ./requirements.txt .

RUN pip install -r requirements.txt