import requests as req
import os
import json
import time

from elasticsearch import Elasticsearch, RequestError
from datetime import datetime
from slug import slug

class App:

    endpoint = None
    token = None
    es = None
    allowed_platforms = []
    indice_name = 'games'
    filter_indice_name = 'filters'
    available_filters = [
        ('genres', 'Gênero'),
        ('platforms', 'Plataforma')
    ]
    added_filters = []

    def __init__(self):
        self.endpoint = os.getenv('API_ENDPOINT')
        self.token = os.getenv('API_KEY')
        allowed_platforms = os.getenv('ALLOWED_PLATFORMS').split(',')
        self.allowed_platforms = [int(id) for id in allowed_platforms]
        self.es = Elasticsearch([{'host': os.getenv('ES_HOST'), 'port': os.getenv('ES_PORT')}])

    def run(self):
        self.create_indices()
        year = 1990
        while (year <= 2020):
            self.get_games_by_year(year)
            year += 1
            time.sleep(10)

    def get_games_by_year(self, year):
        from_date = int(datetime(year, 1, 1).timestamp())
        to_date = int(datetime(year, 12, 31).timestamp())
        allowed_platforms = [str(id) for id in self.allowed_platforms]
        games = []
        offset = 0
        limit = 500

        while True:
            res = req.get(
                f'{self.endpoint}/games',
                headers={'user-key': self.token},
                data=f'''
                    fields id, genres.name, name, platforms.name, platforms.slug, screenshots.url, slug, first_release_date;
                    where release_dates.platform = ({','.join(allowed_platforms)}) & first_release_date >= {from_date} & first_release_date <= {to_date};
                    limit {limit};
                    offset {offset};
                ''',
            )
            games = res.json()
            print('%d - %d - %d' % (year, len(games), offset))
            if (len(games) <= 0):
                break

            for game in games:
                self.add_game(game)

            offset = ((offset + limit) - (offset % limit)) + 1

    def create_indices(self):
        self.create_game_indice()
        self.create_filter_indice()

    def create_filter_indice(self):
        if (self.es.indices.exists(index=self.filter_indice_name) is not True):
            self.es.indices.create(
                index=self.filter_indice_name,
                body={
                    'mappings': {
                        'properties': {
                            'attribute': {
                                'type': 'object'
                            },
                            'value': {
                                'type': 'object'
                            },
                            'updated_at': {
                                'type': 'date'
                            }
                        }
                    }
                }
            )

    def create_game_indice(self):
        if (self.es.indices.exists(index=self.indice_name) is not True):
            self.es.indices.create(
                index=self.indice_name,
                body={
                    'mappings': {
                        'properties': {
                            'release_date': {
                                'type': 'date'
                            },
                            'genres': {
                                'type': 'text'
                            },
                            'name': {
                                'type': 'text'
                            },
                            'platforms': {
                                'type': 'text'
                            },
                            'screenshots': {
                                'type': 'text'
                            },
                            'slug': {
                                'type': 'text'
                            },
                            'facet': {
                                'type': 'text'
                            },
                            'updated_at': {
                                'type': 'date'
                            }
                        }
                    }
                }
            )

    def add_game(self, data):
        data['genres'] = [self.parse_genre(genre) for genre in data['genres']] if 'genres' in data.keys() else []
        data['screenshots'] = [self.parse_screenshots(s) for s in data['screenshots']] if 'screenshots' in data.keys() else []
        data['platforms'] = self.parse_platforms(data['platforms'])

        if (len(data['platforms']) > 0):
            data['facet'] = facet = self.generate_facet(data)
            self.add_game_filter(facet)
            for platform in data['platforms']:
                now = datetime.now()
                id = '%d-%s' % (data['id'], platform['slug'])
                if (id not in self.added_filters):
                    self.es.index(
                        index=self.indice_name,
                        id=id,
                        body={
                            'release_date': data['first_release_date'],
                            'genres': data['genres'],
                            'name': data['name'],
                            'platforms': [plat['name'] for plat in data['platforms']],
                            'screenshots': data['screenshots'],
                            'slug': data['slug'],
                            'facet': facet,
                            'updated_at': datetime.timestamp(now)
                        }
                    )
                    self.added_filters.append(id)

    def add_game_filter(self, facet):
        for filter in facet:
            now = datetime.now()
            [attr, value] = filter.split(':')
            attr_slug = slug(attr)
            value_slug = slug(value)
            id = f'{attr_slug}-{value_slug}'
            self.es.index(
                index=self.filter_indice_name,
                id=id,
                body={
                    'attribute': {
                        'value': attr_slug,
                        'label': attr
                    },
                    'value': {
                        'value': value_slug,
                        'label': value
                    },
                    'updated_at': datetime.timestamp(now)
                }
            )


    def generate_facet(self, game):
        facet = []
        for attr, label in self.available_filters:
            for value in game[attr]:
                value = value['name'] if isinstance(value, dict) else value
                facet.append(f'{label}:{value}')
        return facet

    def parse_genre(self, genre):
        id = genre['id']
        genres = {
            13: 'Simulador',
            24: 'Tático',
            26: 'Quiz',
            4: 'Luta',
            15: 'Estratégia',
            31: 'Aventura',
            12: 'RPG',
            5: 'Tiro',
            7: 'Música',
            16: 'Turnos',
            11: 'RTS',
            25: 'Hack and Slash',
            8: 'Plataforma',
            10: 'Corrida',
            14: 'Esportes'
        }

        return genres[id] if id in genres.keys() else genre['name']

    def parse_screenshots(self, screenshot):
        url = screenshot['url'].replace('t_thumb', 't_original')
        return 'https:' + url

    def parse_platforms(self, platforms):
        output = []
        for platform in platforms:
            if (platform['id'] in self.allowed_platforms):
                output.append(platform)
        return output

app = App()
app.run()